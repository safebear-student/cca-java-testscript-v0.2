package com.safebear.app.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Utils {

    /**
     * Copyright safebear Ltd 2017
     *
     * The Utils page is the one place you go to change any parameters that are used by all the tests,
     * such as the URL of the website.
     *
     * You can also use a 'properties' file if you like for these standard parameters, however it's
     * also useful for standard methods that aren't part of tests but are needed. E.g. we're also
     * using the object to open the website and check it opened correctly.
     *
     */

    // Set up the empty driver
    private WebDriver driver;
    private String url;
    private String browser;
    Properties prop = new Properties();
    InputStream input = null;

    // Getter for the Driver
    public WebDriver getDriver() {
        return driver;
    }

    // Getter for the URL
    public String getUrl() { return url; }


    /** This constructor (below) is a method which runs as soon as the utils object is created in BaseTest
     * All it does is gets the data out of the config.properties file and place it in the variables
     * that we've created at the top of the class. This then means that the data is available for other
     * classes to use
     */
    public Utils(){

        try {

            String filename = "config.properties";
            input = Utils.class.getClassLoader().getResourceAsStream(filename);
            if(input==null){
                System.out.println("Unable to find " + filename);
                return;
            }

            //load a properties file from class path, inside a static method
            prop.load(input);

            //get the property values and place them in the url and browser variables
            this.url = prop.getProperty("url");
            this.browser = prop.getProperty("browser");

        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if(input!=null){
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        // This switch statement sets the browser we'll be using based on the choice made in the config.properties file
        // It is also contained in the Utils() constructor and so it runs when a Utils object is created

        switch(browser) {
            case "chrome":
                this.driver = new ChromeDriver();
                break;
            case "chrome_headless":
                ChromeOptions chromeOptions = new ChromeOptions();
                chromeOptions.addArguments("headless");
                this.driver = new ChromeDriver(chromeOptions);
                break;
            default:
                this.driver = new ChromeDriver();
                break;
        }

    }

}


