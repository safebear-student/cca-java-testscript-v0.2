package com.safebear.app.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WelcomePage {
    /**
     * Copyright safebear Ltd 2017
     *
     * Welcome to the Welcome Page!
     *
     * WebElements:
     * login_link1
     *      Element = link
     *      Locator = linkText
     *
     * Methods:
     * checkCorrectPage()
     *      Inputs: None
     *      Returns: Boolean True or False
     *      WebElements: None
     *      Info: This method confirms that you are on the correct page by checking the title of the
     *      page starts with the word 'Welcome'
     *
     * clickOnLogin()
     *      Inputs: Login Page object
     *      Returns: Boolean True or False
     *      WebElements: login_link
     *      Info: This method clicks on the login link at the top of the page and
     *      confirms that we've moved to the login page
     */

//    Find the login link at the top of the page
    @FindBy(linkText = "Login")
    WebElement login_link;

    //    Create a WebDriver object
    WebDriver driver;

    //    This method is run whenever the welcome page object is created (e.g. in BasePage)
    public WelcomePage(WebDriver driver) {
//        Assign the class instance variable for the WebDriver
        this.driver = driver;
//        Initialise the WebElements
        PageFactory.initElements(driver, this);
    }

    public boolean checkCorrectPage(){

//        Check we're on the correct page by checking what's in the page title
        return driver.getTitle().startsWith("Welcome");
    }

    public void clickOnLogin(){
//        Click on the login link
        login_link.click();
    }
}
