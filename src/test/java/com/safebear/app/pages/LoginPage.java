package com.safebear.app.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class LoginPage {

    /**
     * Copyright safebear Ltd 2017
     *
     * Welcome to the Login Page!
     *
     * WebElements:
     * username_field
     *      Element = field
     *      Locator = id
     * password_field
     *      Element = field
     *      Locator = id
     * input_fields
     *      Element = All input fields (List)
     *      Locator = Xpath
     *
     * Methods:
     * checkCorrectPage()
     *      Inputs: None
     *      Returns: Boolean True or False
     *      WebElements: None
     *      Info: This method will check that you're on the correct page by looking at the page title
     *
     * login()
     *      Inputs: UserPage (object), User Name (String), Password (String)
     *      Returns: Boolean True or False
     *      WebElements: username_field, password_field
     *      Info: This method will use the inputs to log into the application
     *
     * multiElements()
     *      Inputs: Any text (String)
     *      Returns: Boolean True or False
     *      WebElements: input_fields
     *      Info: This method will put the text input into all input fields found on the login screen
     */

//    Find the Username Field
    @FindBy(id = "myid")
    WebElement username_field;

//    Find the Password Field
    @FindBy(id = "mypass")
    WebElement password_field;

//    Find both the Username and Password field by using the fact that both of their IDs contain the word 'my'
    @FindAll({
            @FindBy(xpath = "//input[contains(@id,'my')]")
            })
    List<WebElement> input_fields;

//    Declare a WebDriver object
    WebDriver driver;

//    This method is run whenever the welcome page object is created (e.g. in BasePage)
    public LoginPage(WebDriver driver){

//        Assign the class instance variable for the WebDriver
        this.driver = driver;
//        Initiate all the WebElements
        PageFactory.initElements(driver,this);

    }

    public boolean checkCorrectPage(){

//        Check we're on the correct page by checking what's in the page title
        return driver.getTitle().startsWith("Sign");
    }

    public void login(String uname, String pword){

//        Type the username into the user name field
        this.username_field.sendKeys(uname);
//        Type the password into the password field
        this.password_field.sendKeys(pword);
//        Submit the login form. This can be done using the username_field webelement or the password_field one as below.
        this.password_field.submit();
/*        Then wait for 2 seconds - this is just to demo how you pause for a bit. You need to surround this with a
          'try/catch' statement to ensure that the code continues even if a InterruptedException error is thrown.
 */
        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public boolean multielements(String test){
//        For each of the WebElements in the input_fields list, send the String passed by the variable 'test' (it's "testmulti").
        input_fields.forEach(s -> s.sendKeys(test));
//        Confirm that each field contains 'testmulti' or whatever String is passed.
        return username_field.getAttribute("value").contains(test);
    }

}
