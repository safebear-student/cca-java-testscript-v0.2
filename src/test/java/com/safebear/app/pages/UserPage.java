package com.safebear.app.pages;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class UserPage {

    /**
     * Copyright safebear Ltd 2017
     *
     * Welcome to the User Page!
     *
     * WebElements:
     * javascript_input_button
     *      Element = button
     *      Locator = css
     * javascript_output_field
     *      Element = field
     *      Locator = id
     * frames_button
     *      Element = button
     *      Locator = Xpath
     * first_dropdown
     *      Element = dropdown
     *      Locator = name
     * second_dropdown
     *      Element = dropdown
     *      Locator = name
     * result_field
     *      Element = field
     *      Locator = id
     *
     * Methods:
     * checkCorrectPage()
     *      Inputs: None
     *      Returns: Boolean True or False
     *      WebElements: None
     *      Info: This method will check that you're on the correct page by looking at the page title
     *
     * clickJavascriptInput()
     *      Inputs: None
     *      Returns: Boolean True or False
     *      WebElements: javascript_input_button
     *      Info: This method will click on the Say Something button and confirm that an alert opens
     *
     * enterTextInAlert()
     *      Inputs: String variable - whatever text you want in the alert box
     *      Returns: Boolean True or False
     *      WebElements: javascript_output_field
     *      Info: This method will enter text into the alert and check the text is returned in the
     *      output field.
     *
     * clickOnFramesButton()
     *      Inputs: object Frames Page
     *      Returns: Boolean True or False
     *      WebElements: frames_button
     *      Info: This method will click on the frames button, move to the new window and confirm
     *      that it's the frames window by using the checkCorrectPage() method in the Frames Page
     *
     * addValuesDropdowns()
     *      Inputs: Two integers to add together and the result to check against.
     *      Returns: Boolean True or False
     *      WebElements: first_dropdown, second_dropdown, results_field
     *      Info: This method will use the dropdown boxes to add two numbers together and check the
     *      results when the Submit button is pressed
     */

//    Find the 'Say Something' button
    @FindBy(css = "button.btn.btn-lg.btn-success")
    WebElement javascript_input_button;

//    Find the output field for the 'Say Something' button
    @FindBy(id = "saySomething")
    WebElement javascript_output_field;

//    Find the 'Go to the frames >>' button
    @FindBy(xpath = "//a[contains(text(),'Go to the frames')]")
    WebElement frames_button;

//    Find the first dropdown number field
    @FindBy(name = "num1")
    WebElement first_dropdown;

//    Find the second dropdown number field
    @FindBy(name = "num2")
    WebElement second_dropdown;

//    Find the 'Submit' button
    @FindBy(xpath = "//input[@value='Submit']")
    WebElement dropdown_result_button;

//    Find the result field (the result of adding the two numbers in the dropdowns together)
    @FindBy(id = "addResult")
    WebElement result_field;


//    Create a WebDriver object
    WebDriver driver;
//    Create an Alert object for the pop-up box that appears when you click on 'Say Something'
    Alert javascriptAlert;
//    Create a WebDriverWait object so we can do Explicit waits
    WebDriverWait wait;
//    String for storing the window handle for the current window (the user page) in case we want to return to it
    String user_page_window_handle;
//    Select objects are used for dropdown boxes
    Select select_first_dropdown;
    Select select_second_dropdown;


//    This method is run whenever the welcome page object is created (e.g. in BasePage)
    public UserPage(WebDriver driver) {

//        Assign the class instance variable for the WebDriver
        this.driver = driver;
//        Specify how long the explicit waits will last when you create the 'wait' object
        this.wait = new WebDriverWait(driver,10);
//        Initialise all the Web Elements
        PageFactory.initElements(driver,this);

    }

    public boolean checkCorrectPage() {

//        Check we're on the correct page by checking the title
        return driver.getTitle().startsWith("Logged");
    }

    public boolean clickJavascriptInput(){
//        Click on the 'Say Something' button
        this.javascript_input_button.click();
//        This is an Explicit Wait that will wait 10 seconds to see if the pop-up box appears
//        We need the try/catch statement as if the pop-up doesn't appear, then it errors
        try {
            this.wait.until(ExpectedConditions.alertIsPresent());
        } catch (Exception e) {
            e.printStackTrace();
        }
//        Switch to the pop-up so we can check what's in it
        this.javascriptAlert = driver.switchTo().alert();
//        Confirm it's the correct pop-up box
        return javascriptAlert.getText().contains("What");
    }

    public boolean enterTextInAlert(String saysomething) {

//        Write some text in the pop-up box
        this.javascriptAlert.sendKeys(saysomething);
//        Press ok to submit the text
        this.javascriptAlert.accept();
//        Confirm that the text is returned in the 'Say Something' field
        return javascript_output_field.getText().contains(saysomething);

    }

    public void clickOnFramesButton(){

//        Click on the 'Go to frames' button
        this.frames_button.click();
//        This opens a new tab. This is how we switch to it (using Window Handles)
        this.user_page_window_handle = driver.getWindowHandle();
        for(String window_handle_index : driver.getWindowHandles()){
            driver.switchTo().window(window_handle_index);
        }
    }

    public boolean addValuesDropdowns(int value1, int value2, int result){

//        Create the new Select objects for both dropdowns
        this.select_first_dropdown = new Select(first_dropdown);
        this.select_second_dropdown = new Select(second_dropdown);
//        Change the first dropdown by using the value (we have to convert the integer to a String
//        because selectByValue only accept Strings).
        select_first_dropdown.selectByValue(Integer.toString(value1));
//        Change the second dropdown by using the visible text. Again we have to convert to a String.
        select_second_dropdown.selectByVisibleText(Integer.toString(value2));
//        Click on the 'submit' button
        this.dropdown_result_button.click();
//        Check the result is the addition of both numbers. Again we have to convert.
        return result_field.getText().contains(Integer.toString(result));

    }

}

