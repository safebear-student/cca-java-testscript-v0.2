package com.safebear.app.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MainFrame {

    /**
     * Copyright safebear Ltd 2017
     *
     * Welcome to the Main Frame!
     *
     * Not all Page Objects are pages. This one is a frame within the Frames Page
     * You need a separate Page Object like this because the Web Elements within the
     * frame aren't accessible from the DOM of the Frames Page.
     *
     * WebElements:
     * safebear_logo_image
     *      Element = image
     *      Locator = tag
     *
     * Methods:
     *
     * checkFrame()
     *      Inputs: None
     *      Returns: Nothing currently
     *      WebElements: safebear_logo_image
     *      Info: This method checks we're in the right frame by looking for the safebear logo
     *
     * clickOnLogo()
     *      Inputs: None
     *      Returns: Nothing currently
     *      WebElements: safebear_logo_image
     *      Info: This method clicks on the safebear logo to open the safebear website. It doesn't
     *      return anything currently as I haven't yet created a PageObject for the safebear
     *      website.
     */


//    Find the Safebear Logo in the main frame
    @FindBy(tagName = "img")
    WebElement safebear_logo_image;

//    Declare a WebDriver object
    WebDriver driver;

//    This method is run whenever the welcome page object is created (e.g. in FramesPage)
    public MainFrame (WebDriver driver){

//        Assign the class instance variable for the WebDriver
        this.driver = driver;

//      Initialise the WebElements in this frame
        PageFactory.initElements(driver,this);

    }

    public boolean checkCorrectPage() {

//          Check that the page has the title 'Frame'
        return safebear_logo_image.isDisplayed();
    }


    public void clickOnSafeBearLogo(){
//            Click on the logo to open the SafeBear website
        safebear_logo_image.click();

    }

}
