package com.safebear.app;

import org.junit.Test;
import static org.junit.Assert.assertTrue;

/**
 * Created by sstratton on 08/05/17.
 */
public class Test04_MainPageFrames extends BaseTest {

    /**
     * Copyright safebear Ltd 2017
     *
     * This test will check the Frames Page functionality. See Steps below.
     */

    @Test
    public void testFrames() {
//        Step 1 Confirm we're on the Welcome Page by checking the page title
        assertTrue(welcomePage.checkCorrectPage());
//        Step 2 Click on Login
        welcomePage.clickOnLogin();
//        Step 3 Check we're on the Login page
        assertTrue(loginPage.checkCorrectPage());
//        Step 4 Log in
        loginPage.login("testuser","testing");
//        Step 5 Check we're on the user/logged in page
        assertTrue(userPage.checkCorrectPage());
//        Step 6 Click on the Frames button and switch to frames page
        userPage.clickOnFramesButton();
//        Step 7 Check we're on the frames page
        framesPage.checkCorrectPage();
//        Step 8 Switch to Main Frame
        assertTrue(framesPage.switchToMainFrame());

    }


}
