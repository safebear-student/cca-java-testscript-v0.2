package com.safebear.app;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created by sstratton on 08/05/17.
 */
public class Test01_Login extends BaseTest {

    /**
     * Copyright safebear Ltd 2017
     *
     * This test will login to the website. See Steps below.
     */

    @Test
    public void testLogin(){
//        Step 1 Confirm we're on the Welcome Page
        assertTrue(welcomePage.checkCorrectPage());
//        Step 2 Click on Login link at top of page
        welcomePage.clickOnLogin();
//        Step 3 Check we're on the Login page
        assertTrue(loginPage.checkCorrectPage());
//        Step 4 Login with valid credentials
        loginPage.login("testuser","testing");
//        Step 5 Check we're on the user/logged in page
        assertTrue(userPage.checkCorrectPage());



    }


}
