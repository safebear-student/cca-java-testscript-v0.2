package com.safebear.app;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;



public class TestRunner {
    /**
     * Copyright safebear Ltd 2017
     *
     * This is where you can run all your tests at once from using Test Runner!
     * Test Runner uses the TestSuite object (where the order of the tests are listed
     * in order to run the tests.
     *
     * Try pressing ctrl+shift+F10 and see them run one after the other.
     *
     * @param args
     */

    public static void main(String[] args) {
        Result result = JUnitCore.runClasses(TestSuite.class);
        for (Failure failure : result.getFailures()) {
            System.out.println(failure.toString());
        }

        System.out.println(result.wasSuccessful());

    }

}
