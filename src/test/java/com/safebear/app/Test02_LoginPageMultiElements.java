package com.safebear.app;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class Test02_LoginPageMultiElements extends BaseTest {

    /**
     * Copyright safebear Ltd 2017
     *
     * This test will enter text into multiple elements identified in a List. See Steps below.
     */

    @Test
    public void testMultiElements() {
//        Step 1 Confirm we're on the Welcome Page
        assertTrue(welcomePage.checkCorrectPage());
//        Step 2 Click on Login
        welcomePage.clickOnLogin();
//        Step 3 Check we're on the Login page
        assertTrue(loginPage.checkCorrectPage());
//        Step 4 Identify all the input fields and populate them with 'testmulti'
        assertTrue(loginPage.multielements("testmulti"));
    }

}
