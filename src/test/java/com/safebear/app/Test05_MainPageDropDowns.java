package com.safebear.app;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created by sstratton on 08/05/17.
 */
public class Test05_MainPageDropDowns extends BaseTest {
    /**
     * Copyright safebear Ltd 2017
     *
     * This test will pick numbers from the dropdowns and add them together. See Steps below.
     */

    @Test
    public void testDropdowns() {
//        Step 1 Confirm we're on the Welcome Page by checking the page title
        assertTrue(welcomePage.checkCorrectPage());
//        Step 2 Click on Login
        welcomePage.clickOnLogin();
//        Step 3 Check we're on the Login page
        assertTrue(loginPage.checkCorrectPage());
//        Step 4 Log in
        loginPage.login("testuser","testing");
//        Step 5 Check we're on the user/logged in page
        assertTrue(userPage.checkCorrectPage());
//        Step 6 Enter two values into the dropdowns ('2' and '2'), click add and ensure that the result is '4'
        assertTrue(userPage.addValuesDropdowns(2,2,4));
    }

}
