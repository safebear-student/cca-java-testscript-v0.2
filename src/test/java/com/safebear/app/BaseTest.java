package com.safebear.app;

import com.safebear.app.pages.FramesPage;
import com.safebear.app.pages.LoginPage;
import com.safebear.app.pages.UserPage;
import com.safebear.app.pages.WelcomePage;
import com.safebear.app.utils.Utils;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
//Imports for selenium server so you can run headless
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import java.net.MalformedURLException;
import java.net.URL;




import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;

public class BaseTest {

    /**
     * Copyright safebear Ltd 2017
     *
     * As all of our tests use the same pre-reqs (setUp) and close down (tearDown) we're keeping them
     * in one place: here. This is our BasePage that all other tests inherit.
     *
     * It's also a great place to initialise all your PageObjects and set-up your WebDriver.
     *
     * You can also include your implicit wait time in here and maximise your window.
     *
     */

    WebDriver driver;
    Utils utility;
    LoginPage loginPage;
    WelcomePage welcomePage;
    UserPage userPage;
    FramesPage framesPage;

    @Before
    public void setUp() {

//      Set up utility object
        utility = new Utils();

//      Grab the driver from Utils
        driver = utility.getDriver();

//      Create PageObjects
        loginPage = new LoginPage(driver);
        welcomePage = new WelcomePage(driver);
        userPage = new UserPage(driver);
        framesPage = new FramesPage(driver);

//      Implicit wait
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

//      Go to the website using our Utils method
        driver.get(utility.getUrl());

//      Maximise your window
        driver.manage().window().maximize();

    }

    @After
    public void tearDown(){
//        I put a delay of 1 second in here so you can see for yourself if the tests are successful
//        or not
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        driver.quit();

    }
}
